//$env:JAVA_HOME="C:\Users\Public\wpilib\2022\jdk"

package networktablesclient;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;

public class Client {
    private double turnSpeed;
    private double error;
    private double integral;
    private double derivative;
    private double proportionalAdj;
    private double integralAdj;
    private double derivativeAdj;
    private double leftDist;
    private double rightDist;
    private double speedSpeed;

    private boolean firstCSVPrint;

    private int counter=0;

    public static void main(String[] args){
        new Client().run();
    }

    public void run(){
        firstCSVPrint = true;
        NetworkTableInstance inst = NetworkTableInstance.getDefault();
        NetworkTable table = inst.getTable("/PIDValues");
        NetworkTableEntry turnSpeedEntry = table.getEntry("turnSpeed");
        NetworkTableEntry errorEntry = table.getEntry("error");
        NetworkTableEntry integralEntry = table.getEntry("integral");
        NetworkTableEntry derivativeEntry = table.getEntry("derivative");
        NetworkTableEntry proportionalAdjEntry = table.getEntry("proportionalAdj");
        NetworkTableEntry integralAdjEntry = table.getEntry("integralAdj");
        NetworkTableEntry derivativeAdjEntry = table.getEntry("derivativeAdj");
        NetworkTableEntry leftDistEntry = table.getEntry("leftDist");
        NetworkTableEntry rightDistEntry = table.getEntry("rightDist");
        // NetworkTableEntry speedSpeedEntry = table.getEntry("speedAxis");

        inst.startClientTeam(2996);
        

        while(true){
            try {
            Thread.sleep(20);
        } catch(InterruptedException ex){
            System.out.println("Interrupted");
            return;
            }

            turnSpeed = turnSpeedEntry.getDouble(0);
            error = errorEntry.getDouble(0);
            integral = integralEntry.getDouble(0);
            derivative = derivativeEntry.getDouble(0);
            proportionalAdj = proportionalAdjEntry.getDouble(0);
            integralAdj = integralAdjEntry.getDouble(0);
            derivativeAdj = derivativeAdjEntry.getDouble(0);
            leftDist = leftDistEntry.getDouble(0);
            rightDist = rightDistEntry.getDouble(0);
            // speedSpeed = speedSpeedEntry.getDouble(0);

            counter++;

            // printNWTValues();
            printCSVFormat();
        }
    }

    public void printNWTValues() {
        System.out.println("turnSpeed : " + turnSpeed);
        System.out.println("error : " + error);
        System.out.println("integral : " + integral);
        System.out.println("derivative : " + derivative);
        System.out.println("proportionalAdj : " + proportionalAdj);
        System.out.println("integralAdj : " + integralAdj);
        System.out.println("derivativeAdj : " + derivativeAdj);
        System.out.println("leftDist : " + leftDist);
        System.out.println("rightDist : " + rightDist);
        // System.out.println("speedSpeed : " + speedSpeed);
    }

    public void printCSVFormat() {
        if(firstCSVPrint) {
            System.out.println("time, turnSpeed, error, integral, derivative, proportionalAdj, integralAdj, derivativeAdj, leftDist, rightDist");
            firstCSVPrint = false;
        }

        System.out.println("" + counter + ", " + turnSpeed + ", " + error + ", " + integral + ", " + derivative + ", " + proportionalAdj + ", " + integralAdj + ", " + derivativeAdj + ", " + leftDist + ", " + rightDist);

    }

}
