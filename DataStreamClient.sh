#!/bin/bash

if [ -z $1 ]
then
    file="data.csv"
else
    file=$1
fi
echo -e "\n\e[34mINFO:\e[0m Collected data will be written to \e[1m./data/"$file"\e[0m"

if [ -e ./data/$file ]
then
    echo -e "\n\e[1;31mWARNING: The file ./data/"$file" already exists!\e[0m"
    read -p "    Are you sure you want to overwrite it (y/N)? " -n 1 -r
    if [[ ! $REPLY =~ ^[Yy]$ ]]
    then
        echo -e "  \e[1;31mABORTING\e[0m\n"
        exit 3
    else
        echo -e "  \e[33mContinuing\e[0m"
        rm ./data/$file
    fi
fi

echo -e "\n\e[1;32mRunning\e[0m"
echo -e "    \e[34mINFO:\e[0m Send Ctl + C (SIGINT) to stop program."
echo

# rm -rf ./$1

# ./gradlew run | tee -a ./$1
# ./gradlew run | sed -e '/>/d' | tee -a ./$1
# ./gradlew run | stdbuf -oL sed -e '/>/d;/^$/d' | tee -a ./$1

./gradlew run | stdbuf -oL sed -e '/>/d;/^$/d' > ./data/$file

# ./gradlew run | sed -e '/>/d;/^$/d' | tee -a ./$1

echo
echo -e "\e[1;32mStopped\e[0m\n"
